import unittest

from . import ast, stats_visitor


class TestStatsVisitor(unittest.TestCase):
    def test_one_stats_success(self):
        prg1 = "x := 10; print_state"
        ast1 = ast.parse_string(prg1)

        sv = stats_visitor.StatsVisitor()
        sv.visit(ast1)
        self.assertEqual(sv.get_num_stmts(), 2)
        self.assertEqual(sv.get_num_vars(), 1)

    def test_two_stats_success(self):
        prg1 = "x := 10; assert x < 14; assume x > 0; y := x * 3 / 2 + 1 - 1; "\
               "if x >= y then skip else {if false then skip else y := -1}; skip; print_state; "\
               "z := 10; while z <= 15 inv z > 0 do {if z = x or (true and not false) then z := z * 2 else z := 0}"
        ast1 = ast.parse_string(prg1)

        sv = stats_visitor.StatsVisitor()
        sv.visit(ast1)
        self.assertEqual(sv.get_num_stmts(), 16)
        self.assertEqual(sv.get_num_vars(), 3)

    def test_three_stats_success(self):
        prg1 = "assert 3 + 4 < 2 + 5; havoc x1"
        ast1 = ast.parse_string(prg1)

        sv = stats_visitor.StatsVisitor()
        sv.visit(ast1)
        self.assertEqual(sv.get_num_stmts(), 2)
        self.assertEqual(sv.get_num_vars(), 1)