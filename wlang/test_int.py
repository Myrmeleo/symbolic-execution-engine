# The MIT License (MIT)
# Copyright (c) 2016 Arie Gurfinkel

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import unittest

from . import ast, int


class TestInt(unittest.TestCase):
    def test_one_int_success(self):
        prg1 = "x := 10; print_state"
        # test parser
        ast1 = ast.parse_string(prg1)
        interp = int.Interpreter()
        st = int.State()
        st = interp.run(ast1, st)
        self.assertIsNotNone(st)
        # x is defined
        self.assertIn("x", st.env)
        # x is 10
        self.assertEqual(st.env["x"], 10)
        # no other variables in the state
        self.assertEqual(len(st.env), 1)

    def test_two_int_success(self):
        prg1 = "x := 10; assert x < 14; assume x > 0; y := x * 3 / 2 + 1 - 1; "\
               "if x >= y then skip else {if false then skip else y := -1}; skip; print_state; "\
               "z := 10; while z <= 15 inv z > 0 do {if z = x or (true and not false) then z := z * 2 else z := 0}"
        # test parser
        ast1 = ast.parse_string(prg1)
        ast2 = ast.parse_string(prg1)
        eq = ast1 == ast2
        str1 = str(ast1)
        repr1 = repr(ast1)
        interp = int.Interpreter()
        st = int.State()
        repr2 = st.__repr__()
        st = interp.run(ast1, st)