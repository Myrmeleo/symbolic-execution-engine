# The MIT License (MIT)
# Copyright (c) 2016 Arie Gurfinkel

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from . import ast

class StatsVisitor(ast.AstVisitor):
    """Statistics gathering visitor"""

    def __init__(self):
        super(StatsVisitor, self).__init__()
        # number of statements visited
        self._num_stmts = 0
        # the set of all visited variables
        # set of strings - names of the variables
        self._vars = set()

    def get_num_stmts(self):
        """Returns number of statements visited"""
        return self._num_stmts

    def get_num_vars(self):
        """Returns number of distinct variables visited"""
        return len(self._vars)

    def visit_StmtList(self, node, *args, **kwargs):
        assert isinstance(node, ast.StmtList)
        #visit every statement in the list
        for s in node.stmts:
            visitor1 = StatsVisitor()
            visitor1.visit(s)
            self._num_stmts += visitor1._num_stmts
            self._vars.update(visitor1._vars)

    def visit_Stmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.Stmt)
        #defer to the appropriate visit function below
        #most of these branches won't be executed because super.visit() defers to the functions automatically
        if isinstance(node, ast.AsgnStmt):
            self.visit_AsgnStmt(node, args, kwargs)
        elif isinstance(node, ast.AssertStmt):
            self.visit_AssertStmt(node, args, kwargs)
        elif isinstance(node, ast.AssumeStmt):
            self.visit_AssumeStmt(node, args, kwargs)
        elif isinstance(node, ast.HavocStmt):
            self.visit_HavocStmt(node, args, kwargs)
        elif isinstance(node, ast.IfStmt):
            self.visit_IfStmt(node, args, kwargs)
        elif isinstance(node, ast.PrintStateStmt):
            self._num_stmts += 1
        elif isinstance(node, ast.SkipStmt):
            self._num_stmts += 1
        elif isinstance(node, ast.WhileStmt):
            self.visit_WhileStmt(node, args, kwargs)

    def visit_IntVar(self, node, *args, **kwargs):
        assert isinstance(node, ast.IntVar)
        #variable spotted
        self._vars.add(node.name)

    def visit_Const(self, node, *args, **kwargs):
        assert isinstance(node, ast.Const)
        #no variables or statement to count
        pass

    def visit_AsgnStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.AsgnStmt)
        self._num_stmts += 1
        self._vars.add(node.lhs.name)
        #visit the RHS
        visitor1 = StatsVisitor()
        visitor1.visit(node.rhs)
        self._num_stmts += visitor1._num_stmts
        self._vars.update(visitor1._vars)

    def visit_IfStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.IfStmt)
        self._num_stmts += 1
        #visit the cond
        visitor1 = StatsVisitor()
        visitor1.visit(node.cond)
        self._num_stmts += visitor1._num_stmts
        self._vars.update(visitor1._vars)
        #visit the then
        visitor2 = StatsVisitor()
        visitor2.visit(node.then_stmt)
        self._num_stmts += visitor2._num_stmts
        self._vars.update(visitor2._vars)
        #visit the else
        if node.else_stmt is not None:
            visitor3 = StatsVisitor()
            visitor3.visit(node.else_stmt)
            self._num_stmts += visitor3._num_stmts
            self._vars.update(visitor3._vars)

    def visit_WhileStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.WhileStmt)
        self._num_stmts += 1
        #visit the cond
        visitor1 = StatsVisitor()
        visitor1.visit(node.cond)
        self._num_stmts += visitor1._num_stmts
        self._vars.update(visitor1._vars)
        #visit the inv
        if node.inv is not None:
            visitor2 = StatsVisitor()
            visitor2.visit(node.inv)
            self._num_stmts += visitor2._num_stmts
            self._vars.update(visitor2._vars)
        #visit the body
        visitor3 = StatsVisitor()
        visitor3.visit(node.body)
        self._num_stmts += visitor3._num_stmts
        self._vars.update(visitor3._vars)

    def visit_AssertStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.AssertStmt)
        self._num_stmts += 1
        #visit the asserted expression
        visitor1 = StatsVisitor()
        visitor1.visit(node.cond)
        self._num_stmts += visitor1._num_stmts
        self._vars.update(visitor1._vars)

    def visit_AssumeStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.AssumeStmt)
        self._num_stmts += 1
        #visit the assumed expression
        visitor1 = StatsVisitor()
        visitor1.visit(node.cond)
        self._num_stmts += visitor1._num_stmts
        self._vars.update(visitor1._vars)

    def visit_HavocStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.HavocStmt)
        self._num_stmts += 1
        #add variables in the statement
        self._vars.update([x.name for x in node.vars])

    def visit_Exp(self, node, *args, **kwargs):
        assert isinstance(node, ast.Exp)
        #go through all arguments and count the variables
        if isinstance(node, ast.AExp) or isinstance(node, ast.BExp):
            for i in node.args:
                visitor1 = StatsVisitor()
                visitor1.visit(i)
                self._num_stmts += visitor1._num_stmts
                self._vars.update(visitor1._vars)
        elif isinstance(node, ast.RelExp):
            #visit the LHS
            visitor1 = StatsVisitor()
            visitor1.visit(node.args[0])
            self._num_stmts += visitor1._num_stmts
            self._vars.update(visitor1._vars)
            #visit the RHS
            visitor2 = StatsVisitor()
            visitor2.visit(node.args[1])
            self._num_stmts += visitor2._num_stmts
            self._vars.update(visitor2._vars)


def main():
    import sys

    prg = ast.parse_file(sys.argv[1])
    sv = StatsVisitor()
    sv.visit(prg)
    print("stmts:", sv.get_num_stmts(), "vars:", sv.get_num_vars())


if __name__ == "__main__":
    import sys

    sys.exit(main())
