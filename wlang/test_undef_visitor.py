import unittest

from . import ast, undef_visitor


class TestUndefVisitor(unittest.TestCase):
    def test_one_undef_success(self):
        prg1 = "x := 10; y := x + z"
        ast1 = ast.parse_string(prg1)

        uv = undef_visitor.UndefVisitor()
        uv.check(ast1)
        self.assertEqual({ast.IntVar('z')}, uv.get_undefs ())

    def test_two_undef_success(self):
        prg1 = "x := 3; if 2 + 2 = 4 then x:= 4 else x := 5"
        ast1 = ast.parse_string(prg1)

        uv = undef_visitor.UndefVisitor()
        uv.check(ast1)
        self.assertSetEqual(set(), uv.get_undefs())

    def test_three_undef_success(self):
        prg1 = "a := b + c - d / e * f; havoc x1"
        ast1 = ast.parse_string(prg1)

        uv = undef_visitor.UndefVisitor()
        uv.check(ast1)
        self.assertSetEqual({ast.IntVar('b'), ast.IntVar('c'), ast.IntVar('d'), ast.IntVar('e'), ast.IntVar('f')},
                            uv.get_undefs())