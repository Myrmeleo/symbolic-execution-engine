# The MIT License (MIT)
# Copyright (c) 2016 Arie Gurfinkel

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from . import ast


class UndefVisitor(ast.AstVisitor):
    """Counts all variables that are used in a program before being defined"""

    def __init__(self):
        super(UndefVisitor, self).__init__()
        # the set of all variables in the program used before definition
        # set of ast.IntVar objects
        self.undefined_vars = set()
        # the set of defined variables in the program, updated as we go through the AST
        # set of ast.IntVar objects
        self.defined_vars = set()
        #required to be true before This can return its above sets
        self.has_checked = False

    def check(self, node):
        """Check for undefined variables starting from a given AST node"""
        self.visit(node)
        self.has_checked = True

    def get_undefs(self):
        """Return the set of all variables that are used before being defined
        in the program.  Available only after a call to check()
        """
        if not self.has_checked: return None
        return self.undefined_vars

    def visit_IntVar(self, node, *args, **kwargs):
        assert isinstance(node, ast.IntVar)
        #add variable to set of undefineds for now; this function is only called recursively, and the caller can remove later (see visit_StmtList)
        self.undefined_vars.add(node)

    def visit_Const(self, node, *args, **kwargs):
        assert isinstance(node, ast.Const)
        #no variables to count
        pass

    def visit_Exp(self, node, *args, **kwargs):
        assert isinstance(node, ast.Exp)
        #go through all arguments and take the variables
        if isinstance(node, ast.AExp) or isinstance(node, ast.BExp):
            for i in node.args:
                visitor1 = UndefVisitor()
                visitor1.visit(i)
                #as with IntVars, mark all vars as undefineds indiscriminately
                #this function is only called recursively, and the caller will remove defined vars later (see visit_StmtList)
                self.undefined_vars.update(visitor1.undefined_vars)
        elif isinstance(node, ast.RelExp):
            #visit the LHS
            visitor1 = UndefVisitor()
            visitor1.visit(node.args[0])
            self.undefined_vars.update(visitor1.undefined_vars)
            #visit the RHS
            visitor2 = UndefVisitor()
            visitor2.visit(node.args[1])
            self.undefined_vars.update(visitor2.undefined_vars)

    def visit_StmtList(self, node, *args, **kwargs):
        assert isinstance(node, ast.StmtList)
        #visit every statement in the list
        for s in node.stmts:
            visitor1 = UndefVisitor()
            visitor1.visit(s)
            #add defined variables
            self.defined_vars.update(visitor1.defined_vars)
            #if the visitor has an 'undefined' variable that was defined in an earlier statement, we don't count it
            self.undefined_vars.update(visitor1.undefined_vars.difference(self.defined_vars))

    def visit_Stmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.Stmt)
        """
        Defer to the appropriate visit function below
        
        Most of these branches won't be executed because super.visit() defers to the functions automatically
        
        In each of these functions that are called, we will count the number of variables that are used the in the
        statement without being defined earlier in that same statement. If any of these variables were defined in an
        earlier statement, visit_StmtList will remove them.
        """
        if isinstance(node, ast.AsgnStmt):
            self.visit_AsgnStmt(node, args, kwargs)
        elif isinstance(node, ast.AssertStmt):
            self.visit_AssertStmt(node, args, kwargs)
        elif isinstance(node, ast.AssumeStmt):
            self.visit_AssumeStmt(node, args, kwargs)
        elif isinstance(node, ast.HavocStmt):
            self.visit_HavocStmt(node, args, kwargs)
        elif isinstance(node, ast.IfStmt):
            self.visit_IfStmt(node, args, kwargs)
        elif isinstance(node, ast.PrintStateStmt):
            pass
        elif isinstance(node, ast.SkipStmt):
            pass
        elif isinstance(node, ast.WhileStmt):
            self.visit_WhileStmt(node, args, kwargs)

    def visit_AsgnStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.AsgnStmt)
        #add the defined variable to the list of defined variables
        self.defined_vars.add(node.lhs)
        #visit the RHS and take all its variables
        visitor1 = UndefVisitor()
        visitor1.visit(node.rhs)
        self.undefined_vars.update(visitor1.undefined_vars)

    def visit_HavocStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.HavocStmt)
        #all variables are being defined here
        self.defined_vars.update(node.vars)

    def visit_AssertStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.AssertStmt)
        #visit the asserted expression and take all its variables
        visitor1 = UndefVisitor()
        visitor1.visit(node.cond)
        self.undefined_vars.update(visitor1.undefined_vars)

    def visit_AssumeStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.AssumeStmt)
        #visit the assumed expression and take all its variables
        visitor1 = UndefVisitor()
        visitor1.visit(node.cond)
        self.undefined_vars.update(visitor1.undefined_vars)

    def visit_IfStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.IfStmt)
        #visit the cond
        visitor1 = UndefVisitor()
        visitor1.visit(node.cond)
        self.undefined_vars.update(visitor1.undefined_vars)
        #visit the then
        visitor2 = UndefVisitor()
        visitor2.visit(node.then_stmt)
        then_defined_vars = visitor2.defined_vars
        self.undefined_vars.update(visitor2.undefined_vars)
        #visit the else
        if node.else_stmt is not None:
            visitor3 = UndefVisitor()
            visitor3.visit(node.else_stmt)
            else_defined_vars = visitor3.defined_vars
            self.undefined_vars.update(visitor3.undefined_vars)

            #only variables that are defined in both the then and else branches can be considered defined
            both_branch_vars = then_defined_vars.intersection(else_defined_vars)
            #however, such a var is still undefined if it's in the cond, since it appears before definition
            self.defined_vars.update(both_branch_vars.difference(visitor1.undefined_vars))

    def visit_WhileStmt(self, node, *args, **kwargs):
        assert isinstance(node, ast.WhileStmt)
        #visit the cond
        visitor1 = UndefVisitor()
        visitor1.visit(node.cond)
        self.undefined_vars.update(visitor1.undefined_vars)
        #visit the inv
        if node.inv is not None:
            visitor2 = UndefVisitor()
            visitor2.visit(node.inv)
            self.undefined_vars.update(visitor2.undefined_vars)
        #visit the body
        visitor3 = UndefVisitor()
        visitor3.visit(node.body)
        #since there's no guarantee the loop will run, no variables defined in it are counted
        ###self.defined_vars.update(visitor3.defined_vars)
        self.undefined_vars.update(visitor3.undefined_vars)
