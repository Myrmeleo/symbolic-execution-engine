# The MIT License (MIT)
# Copyright (c) 2016 Arie Gurfinkel

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys

import io 
import z3

from . import ast, int
from functools import reduce


#Symbolic execution state
class SymState(object):
    def __init__(self, solver=None):
        # environment mapping variables to symbolic constants
        self.env = dict()
        # path condition
        self.path = list()
        self._solver = solver
        if self._solver is None:
            self._solver = z3.Solver()

        # true if this is an error state
        self._is_error = False

    def add_pc(self, *exp):
        """Add constraints to the path condition"""
        self.path.extend(exp)
        self._solver.append(exp)

    def is_error(self):
        return self._is_error

    def mk_error(self):
        self._is_error = True

    def is_empty(self):
        """Check whether the current symbolic state has any concrete states"""
        res = self._solver.check()
        return res == z3.unsat

    def pick_concrete(self):
        """Pick a concrete state consistent with the symbolic state.
           Return None if no such state exists"""
        res = self._solver.check()
        if res != z3.sat:
            return None
        model = self._solver.model()
        st = int.State()
        for (k, v) in self.env.items():
            st.env[k] = model.eval(v)
        return st

    def fork(self):
        """Fork the current state into two identical states that can evolve separately"""
        child = SymState()
        child.env = dict(self.env)
        child.add_pc(*self.path)

        return (self, child)

    def __repr__(self):
        return str(self)

    def to_smt2(self):
        """Returns the current state as an SMT-LIB2 benchmark"""
        return self._solver.to_smt2()

    def __str__(self):
        buf = io.StringIO()
        for k, v in self.env.items():
            buf.write(str(k))
            buf.write(': ')
            buf.write(str(v))
            buf.write('\n')
        buf.write('pc: ')
        buf.write(str(self.path))
        buf.write('\n')

        return buf.getvalue()


#Symbolic executioner - runs a program and returns a list of symbolic states corresponding to ends of possible execution paths
class SymExec(ast.AstVisitor):
    def __init__(self):
        #the list of symbolic states to return
        self.states = list()

    #state is typically a newly initialized state
    def run(self, ast, state):
        #start with a state corresponding to the start of the program, will go through the program and modify/fork as needed
        self.states.append(state)
        self.visit(ast, state=state)
        return self.states
    
    """
    Next 3 funtions give z3 representations of vars and consts
    """
    def visit_IntVar(self, node, *args, **kwargs):
        return z3.Int(node.name)

    def visit_BoolConst(self, node, *args, **kwargs):
        return z3.BoolVal(node.val)

    def visit_IntConst(self, node, *args, **kwargs):
        return z3.IntVal(node.val)

    """
    Next 3 funtions translate expressions into their z3 equivalents
    """
    def visit_RelExp(self, node, *args, **kwargs):
        lhs = self.visit(node.args[0], *args, **kwargs)
        rhs = self.visit(node.args[1], *args, **kwargs)
        if node.op == "<=":
            return z3.And(lhs <= rhs)
        if node.op == "<":
            return z3.And(lhs < rhs)
        if node.op == "=":
            return z3.And(lhs == rhs)
        if node.op == ">=":
            return z3.And(lhs >= rhs)
        if node.op == ">":
            return z3.And(lhs > rhs)
        assert False

    def visit_BExp(self, node, *args, **kwargs):
        kids = [self.visit(a, *args, **kwargs) for a in node.args]
        if node.op == "not":
            assert node.is_unary()
            assert len(kids) == 1
            return z3.Not(kids[0])
        elif node.op == "and":
            return z3.And(x for x in kids)
        elif node.op == "or":
            return z3.Or(x for x in kids)

    def visit_AExp(self, node, *args, **kwargs):
        kids = [self.visit(a, *args, **kwargs) for a in node.args]
        res = kids[0]
        if len(kids) < 2: return res
        if node.op == "+":
            return z3.Sum(kids)
        elif node.op == "-":
            return reduce(lambda x, y: x - y, kids)
        elif node.op == "*":
            return z3.Product(kids)
        elif node.op == "/":
            return reduce(lambda x, y: x / y, kids)
        return z3.IntVal(res)

    """
    Now we use the z3 expressions/values to go through the statements and modify path conditions
    All functions will return the states after execution, so that SymExec can replace the old version with the new
    """
    def visit_SkipStmt(self, node, *args, **kwargs):
        return kwargs["state"]

    def visit_PrintStateStmt(self, node, *args, **kwargs):
        return kwargs["state"]

    def visit_AsgnStmt(self, node, *args, **kwargs):
        st = kwargs["state"]
        lhs = self.visit(node.lhs, *args, **kwargs)
        rhs = self.visit(node.rhs, *args, **kwargs)
        st.env[node.lhs.name] = rhs
        return st

    def visit_IfStmt(self, node, *args, **kwargs):
        st = kwargs["state"]
        cond = self.visit(node.cond, *args, **kwargs)
        #functions handling control flow statements will return multiple states - one for each outcome
        return_states = []
        #2 states - one for each outcome of the if cond
        branch1, branch2 = st.fork()
        branch1.add_pc(cond)
        branch2.add_pc(z3.Not(cond))

        #let branch1 be modified as it runs through the body of the statement
        result_state = self.visit(node.then_stmt, state=branch1)
        #maybe the body of code executed was another control flow statement, in which case we end up with multiple states
        if isinstance(result_state, list):
            return_states.extend(result_state)
        else:
            return_states.append(result_state)
        
        if node.has_else():
            #let branch 2 be modified as it runs through the else of the statement
            result_state = self.visit(node.else_stmt, state=branch2)
            if isinstance(result_state, list):
                return_states.extend(result_state)
            else:
                return_states.append(result_state)
        else:
            return_states.append(branch2)

        return return_states

    def visit_WhileStmt(self, node, *args, **kwargs):
        st = kwargs["state"]
        return_states = [st]
        cond = self.visit(node.cond, *args, **kwargs)
        st.add_pc(cond)
        """
        return_states will have states corresponding to each number of iterations executed, up to 10 iterations
        as we add states to return_states, we will take the states with the highest number of iterations passed so far, and run them through the loop
        again to get the next iteration of states
        
        maybe a single execution of the body will result in branching execution paths (if the body has a control flow statement)
        so we can't just take the last element in return_states, but we must take all of them with the highest number of iterations
        those states will be appended to the end of the list, so it's easy to track how many there are
        """
        #count how many states in return_states don't need to be run though the loop again
        finished_states_count = 0
        #assume that the loop will not run more than 10 times
        for i in range(10):
            current_len = len(return_states)
            #simulate a run through the body of the loop, with all states that have passed the body the most so far
            for s in range(finished_states_count, current_len):
                result_state = self.visit(node.body, state=return_states[s])
                #maybe the body of code executed was another control flow statement, in which case we end up with multiple result states
                if isinstance(result_state, list):
                    for s in return_states:
                        #we are assuming that all execution paths escape the loop eventually
                        s.add_pc(cond)
                    return_states.extend(result_state)
                else:
                    result_state.add_pc(cond)
                    return_states.append(result_state)
            finished_states_count = current_len
        return return_states

    def visit_AssertStmt(self, node, *args, **kwargs):
        cond = self.visit(node.cond, *args, **kwargs)
        st = kwargs["state"]
        st.add_pc(cond)
        if st.is_empty():
            st.mk_error()
        return st

    def visit_AssumeStmt(self, node, *args, **kwargs):
        return self.visit_AssertStmt(node, *args, **kwargs)

    def visit_HavocStmt(self, node, *args, **kwargs):
        st = kwargs["state"]
        for v in node.vars:
            res = self.visit(v, *args, **kwargs)
            st.env[v.name] = res
        return st

    #now for the whole program, run all states through each statement in the list
    #no need to worry about control flow and if an execution path might not meet one of the statements in the program - this is handled in the for/while functions
    def visit_StmtList(self, node, *args, **kwargs):
        for stmt in node.stmts:
            #the number of states is dynamic, so we need a variable to track how many to run at once to prevent an infinite loop
            num_states = len(self.states)
            for st in self.states[:num_states]:
                result_state = self.visit(stmt, state=st)
                #replace old states with new ones, so the only states remaining are those at the end of execution
                self.states.remove(st)
                if isinstance(result_state, list):
                    self.states.extend(result_state)
                else:
                    self.states.append(result_state)
        return self.states


def _parse_args():
    import argparse
    ap = argparse.ArgumentParser(prog='sym',
                                 description='WLang Interpreter')
    ap.add_argument('in_file', metavar='FILE',
                    help='WLang program to interpret')
    args = ap.parse_args()
    return args


def main():
    args = _parse_args()
    prg = ast.parse_file(args.in_file)
    st = SymState()
    sym = SymExec()
    states = sym.run(prg, st)
    if states is None:
        print('[symexec]: no output states')
    else:
        count = 0
        for out in states:
            count = count + 1
            print('[symexec]: symbolic state reached')
            print(out)
        print('[symexec]: found', count, 'symbolic states')
    return 0


if __name__ == '__main__':
    sys.exit(main())
